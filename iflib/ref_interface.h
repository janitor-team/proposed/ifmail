/* Projet References -- Definitions d'interface
 * Auteur: Marc SCHAEFER <schaefer@alphanet.ch>
 * Date creation: 19/02/96
 * Date modification: 14/06/97
 * V1.0 PV002 MSC96
 * DESCRIPTION
 *    Ceci d�crit de mani�re simpliste l'interface des
 *    fonctions du module ref.c
 * NOTES
 *    - ref is now able to handle more than one open ref database at the
 *      same time.
 * BUGS
 *    - Incomplete prototypes.
 * TODO
 *    - Ajouter les sp�cifications de fonctions.
 * MODIFICATION-HISTORY
 *    PV002 14/06/97  schaefer  Ajout� nom de fichier dbm en param�tre �
 *                              ref_init().
 */

#ifndef REF_IF_H
#define REF_IF_H
#ifndef _TIME_H
#include <time.h>
#endif
#include "ftnmsg.h"

/* Type opaque */
typedef void ref_private_t;

/* NAME
 *    ref_init
 * DESCRIPTION
 *    Opens a specific DBM database implemented as a ref_db database.
 * RESULT
 *    NULL if error. Else ref_private_t *
 * NOTES
 * BUGS
 * TODO
 */
ref_private_t *ref_init(char *dbm_file_name);

/* NAME
 *    ref_store
 * DESCRIPTION
 *    Stores a new association between two strings and sets the
 *    date to be now on that association.
 * RESULT
 *    !0 if ok.
 * NOTES
 * BUGS
 * TODO
 */
int ref_store(ref_private_t *, char *, char *);

/* NAME
 *    ref_store_extended
 * DESCRIPTION
 *    Stores a new association between two strings and the date.
 * RESULT
 *    -1 if ok.
 * NOTES
 *    - Should only be used in case we want to compact the database.
 * BUGS
 * TODO
 */
int ref_store_extended(ref_private_t *, char *, char *, time_t);

/* NAME
 *    ref_get
 * DESCRIPTION
 *    Gets the association from the first parameter.
 * RESULT
 *    0 if not found or database not open. -1 if ok
 * NOTES
 * BUGS
 * TODO
 */
int ref_get(ref_private_t *, char *, char *, int);

/* NAME
 *    ref_get_extended
 * DESCRIPTION
 *    Gets the association from the first parameter.
 * RESULT
 *    0 if not found or database not open. -1 if OK.
 * NOTES
 *    - Use only for expire by copying.
 * BUGS
 * TODO
 */
int ref_get_extended(ref_private_t *, char *, char *, int, time_t *);

/* NAME
 *    ref_first_key
 * DESCRIPTION
 *    Gets the first key.
 * RESULT
 *    0 if not found or database not open. -1 if OK.
 * NOTES
 * BUGS
 * TODO
 */
int ref_first_key(ref_private_t *, char *, int);

/* NAME
 *    ref_next_key
 * DESCRIPTION
 *    Gets the next key.
 * RESULT
 *    0 if not found or database not open. -1 if OK.
 * NOTES
 * BUGS
 * TODO
 */
int ref_next_key(ref_private_t *, char *, int);

/* NAME
 *    ref_deinit
 * DESCRIPTION
 *    Closes the specified ref database.
 * RESULT
 *    NONE
 * NOTES
 * BUGS
 * TODO
 */
void ref_deinit(ref_private_t *);

/* NAME
 *    ref_cleanup
 * DESCRIPTION
 *    Deletes old entries.
 * RESULT
 * NOTES
 *    - Does not compact the database.
 * BUGS
 * TODO
 */
int ref_cleanup(ref_private_t *, char *);

/* NAME
 *    ref_store_msgid
 * DESCRIPTION
 *    Front end for ref_store_extended
 * RESULT
 * NOTES
 * BUGS
 * TODO
 */
int ref_store_msgid(ref_private_t *ref_dbase, ftnmsg *fmsg, char *p);

#endif
