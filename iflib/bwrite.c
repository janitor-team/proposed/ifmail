/* ### Modified by P.Saratxaga on 29 Oct 1995 ###
 * - deleted transcodage (outtab & intab) code (now transcodage is done by
 *   strconv() function.
 */
#include <stdio.h>
#include <string.h>
#include "bwrite.h"
#include "config.h"

/* write short (16bit) integer in "standart" byte order */
int iwrite(i,fp)
int i;
FILE *fp;
{
	putc(i & 0xff,fp);
	putc((i >> 8) & 0xff,fp);
	return 0;
}

/* write long (32bit) integer in "standart" byte order */
int lwrite(i,fp)
long i;
FILE *fp;
{
	int c;

	for (c=0;c<32;c+=8) putc((i >> c) & 0xff,fp);
	return 0;
}

int awrite(s,fp)
char *s;
FILE *fp;
{
	if (s) while (*s) putc(*(s++), fp);
	putc(0,fp);
	return 0;
}

/* write an arbitrary line to message body: change \n to \r,
   if a line starts with three dashes, insert a dash and a blank */
int cwrite(s,fp)
char *s;
FILE *fp;
{
#ifdef PGP_LIKE_DASHES
	if ((strlen(s) >= 3) && (strncmp(s,"---",3) == 0) && (s[3] != '-')) {
		putc('-',fp);
		putc(' ',fp);
	}
#endif
	while (*s) 
	{
		if (*s == '\n') putc('\r',fp);
		else putc(*s, fp);
		s++;
	}
	return 0;
}

/* write (multiline) header to kluge: change \n to ' ' and end line with \r */
int kwrite(s,fp)
char *s;
FILE *fp;
{
	while (*s) 
	{
		if (*s != '\n') putc(*s, fp);
		else if (*(s+1)) putc(' ',fp);
		s++;
	}
	putc('\r',fp);
	return 0;
}
