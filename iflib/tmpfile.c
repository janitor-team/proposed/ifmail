/* Contributed by Igor Sharfmesser <igor@kit.kz> */

#include <stdio.h>
#include <unistd.h>

FILE *tmpfile()
{
  char *name;
  FILE *file;

  name = tempnam("/tmp", "ifm");
  if(name)
  {
    file = fopen(name,"w+");
    if( file )
    {
      unlink(name);
      return file;
    }
  }
  return NULL;
}
