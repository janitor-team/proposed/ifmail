#include <string.h>

#ifdef SETPROCTITLE
/*
 * clobber argv so ps will show what we're doing.
 * (stolen from BSD ftpd where it was stolen from sendmail)
 * warning, since this is usually started from inetd.conf, it
 * often doesn't have much of an environment or arglist to overwrite.
 */

static char *cmdstr=NULL;
static char *cmdstrend=NULL;

void setargspace(argv,envp)
char *argv[];
char *envp[];
{
	cmdstr=argv[0];
	while (*envp) envp++;
	envp--;
	cmdstrend=(*envp)+strlen(*envp);
}

void setproctitle(str)
char *str;
{
	char *p;

	/* make ps print our process name */
	for (p=cmdstr;(p < cmdstrend) && (*str);p++,str++) *p=*str;

	while (p < cmdstrend) *p++ = ' ';
}

#else

void setargspace(argv,envp)
char *argv[];
char *envp[];
{}

void setproctitle(str)
char *str;
{}

#endif
