/* Projet References -- Effacement des entrees expirees de la base.
 * Auteur: Marc SCHAEFER <schaefer@alphanet.ch>
 * Date creation: 01/12/96
 * Date modification: 16/06/97
 * V1.0 PV003 MSC97
 * DESCRIPTION
 *    Ce programme supprime les entrees de la base de references, en
 *    fonction de l'implementation de cette suppression dans ref.c.
 *    Ce module est une interface (wrapper).
 *    A l'aide de la fonction ref_cleanup() supprime les
 *    vieilles entrees dans la base.
 * RESULT
 *    0 si ok
 *    1 si error
 *    2 si pas de dbm
 * NOTES
 * BUGS
 *    - Hard-coded constants; maybe should use lib.a
 *    - See ref.c
 * TODO
 * BASED-ON
 *    refcleanup.c PV001 of 19/02/96 MSC96
 * MODIFICATION-HISTORY
 *    PV002  14/06/97  schaefer  Interface change in ref_interface.h
 */

#include <stdio.h>
#include "config.h"
#include "ref_interface.h"

#define HISTORY_FILE "not_used"
#ifndef REF_DBM
#define REF_DBM "/var/spool/ifmail/refdb"
#endif

int main(void) {
   ref_private_t *ref_db = ref_init(REF_DBM);
   if (ref_db) {
      if (ref_cleanup(ref_db, HISTORY_FILE)) {
         ref_deinit(ref_db);
         exit(0);
         /* NOT REACHED */
      }
      else {
         perror("can't cleanup");
         ref_deinit(ref_db);
         exit(1);
         /* NOT REACHED */
      }
      /* NOT REACHED */
   }
   else {
     perror("could not open ref_db");
     exit(2);
     /* NOT REACHED */
   }
   /* NOT REACHED */
}


