#include <sys/types.h>
#include <unistd.h>
#include <stdio.h>
#include <sys/wait.h>

#define MKDIR "/bin/mkdir"
#define DEVNULL "/dev/null"

int mkdir(char *);
