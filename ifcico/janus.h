#ifndef JANUS_H
#define JANUS_H

#define JS_DONE		0
#define JS_SENDFNAME	1
#define JS_RCVFNACK	2
#define JS_SENDBLK	3
#define JS_RCVEOFACK	4
#define JS_SENDFREQNAK	5
#define JS_RCVFRNAKACK	6

#define JR_DONE		0
#define JR_RCVFNAME	1
#define JR_RCVBLK	2

#define JPKT_NONE	 0
#define JPKT_BAD	'@'
#define JPKT_FNAME	'A'
#define JPKT_FNACK	'B'
#define JPKT_BLK	'C'
#define JPKT_RPOS	'D'
#define JPKT_EOFACK	'E'
#define JPKT_HALT	'F'
#define JPKT_HALTACK	'G'
#define JPKT_FREQ	'H'
#define JPKT_FREQNAK	'I'
#define JPKT_FRNAKACK	'J'

#define JDLM_START	'a'
#define JDLM_END	'b'
#define JDLM_START32	'c'

#define JCAP_CRC32	0x80
#define JCAP_FREQ	0x40

#endif
