/*
 * Copyright (c) 1995 Boris V. Tobotras
 * boris@xtalk.msk.su AKA 2:5020/20.7 AKA 2:5020/188.21
 *
 * Created: Fri Feb  3 14:28:16 1995
 *
 * Txy flag (FSC-0062) processing functions for ifcico
 *
 * Consider code below as freeware, please.
 *
 */
# include	<ctype.h>
# include	<time.h>
# include	"lutil.h"
# include	"Txy.h"

static int IsNodeCallable( char *xy )
{
	time_t GMTime = time( NULL );
	struct tm *Now = gmtime( &GMTime );
	int NowMin = Now->tm_hour * 60 + Now->tm_min;
	int WorkTimeStartMin = ( toupper( *xy ) - 'A' ) * 60,
		WorkTimeEndMin;
	if ( islower( *xy++ ) )
		WorkTimeStartMin += 30;
	WorkTimeEndMin = ( toupper( *xy ) - 'A' ) * 60;
	if ( islower( *xy ) )
		WorkTimeEndMin += 30;
	debug( 10, "Working time: %d:%d-%d:%d, now %d:%d GMT", 
		WorkTimeStartMin / 60, WorkTimeStartMin % 60,
		WorkTimeEndMin / 60, WorkTimeEndMin % 60,
		NowMin / 60, NowMin % 60 ); 
	
	if ( WorkTimeEndMin > WorkTimeStartMin )
		if ( NowMin >= WorkTimeStartMin && NowMin < WorkTimeEndMin )
			return 1;
	    else
			return 0;
	else
		if ( NowMin >= WorkTimeStartMin || NowMin < WorkTimeEndMin )
			return 1;
		else
			return 0;
}

/*
   Checks nodelist entry against U,Txy flag and determines
   whether node can be dialed. If one not present, assumes
   'no'. If Txy found, checks it against current time.
   Returns: 1 if system should NOT be called, 0 otherwise.
   */
int not_work_time_now( nlent )
	node *nlent;
{
	int i;

	if ( nlent->flags & CM ) {
		debug( 10, "Calling CM node" );
		return 0;
	}
	
	for ( i = 0; i < MAXUFLAGS; ++i ) {
		char *flag = nlent->uflags[ i ];
		if ( flag ) {
			if ( flag[ 0 ] == 'T' && flag[ 3 ] == '\0' ) {
				debug( 10, "Found Txy flag: %s", flag++ );
				return IsNodeCallable( flag ) ? 0 : 1;
			} else if ( flag[ 0 ] == 'U' && flag[ 1 ] == 'T' &&
				      flag[ 4 ] == '\0' ) {
				flag++;
				debug( 10, "Found Txy flag: %s", flag++ );
				return IsNodeCallable( flag ) ? 0 : 1;
			}
		}
		else
			break;
	}
	debug( 10, "No Txy flag" );
	/* No Txy flag... defaulting to ZMH */
	/* points don't have to honour ZMH */
	if (nlent->addr.point!=0) {
		if (nlent->addr.zone==1) {
			debug(10,"Using Z1MH");
			return IsNodeCallable( "TJK" ) ? 0 : 1;
		} else if (nlent->addr.zone==2) {
			debug(10,"Using Z2MH");
			return IsNodeCallable( "Tcd" ) ? 0 : 1;
		} else if (nlent->addr.zone==3) {
			debug(10,"Using Z3MH");
			return IsNodeCallable( "TST" ) ? 0 : 1;
		} else if (nlent->addr.zone==4) {
			debug(10,"Using Z4MH");
			return IsNodeCallable( "TIJ" ) ? 0 : 1;
		} else if (nlent->addr.zone==5) {
			debug(10,"Using Z5MH");
			return IsNodeCallable( "TBC" ) ? 0 : 1;
		} else if (nlent->addr.zone==6) {
			debug(10,"Using Z6MH");
			return IsNodeCallable( "TUV" ) ? 0 : 1;
		} else {
			debug( 10, "node to call is not on fidonet");
		}
	}
	return 0;
}
