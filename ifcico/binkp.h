/* protocol version */
#define BINKP_VERSION "1.1"

#define M_NUL	0
#define M_ADR	1
#define M_PWD	2
#define M_FILE	3
#define M_OK	4
#define M_EOB	5
#define M_GOT	6
#define M_ERR	7
#define M_BSY	8
#define M_GET	9
#define M_SKIP	10

#define M_DATA  0xff

#define BINKP_DATA_BLOCK	0x0000
#define BINKP_CONTROL_BLOCK	0x8000

typedef struct _binkp_frame {
	unsigned INT16 header;
	unsigned char id;
	unsigned char *data;
} binkp_frame;

