/* ### Modified by P.Saratxaga on 5 Sep 1995 ### 
 * - I change the way ifmail recognizes archivers format, and added unrar
 */
#include <stdio.h>
#include <string.h>
#include "lutil.h"
#include "config.h"

char *unpacker(fn)
char *fn;
{
	FILE *fp;
	unsigned char buf[24],dbuf[240];
	int i;

	if ((fp=fopen(fn,"r")) == NULL) 
	{
		logerr("$Could not open file %s",S(fn));
		return NULL;
	}
	if (fread(buf,1,sizeof(buf),fp) != sizeof(buf))
	{
		logerr("$Could not read head of the file %s",S(fn));
		return NULL;
	}
	fclose(fp);
	dbuf[0]='\0';
	for (i=0;i<sizeof(buf);i++)
		if ((buf[i] >= ' ') && (buf[i] <= 127)) 
			sprintf((char*)dbuf+strlen(dbuf),"  %c",buf[i]);
		else
			sprintf((char*)dbuf+strlen(dbuf)," %02x",buf[i]);
	debug(2,"file head: %s",dbuf);

/* Changed checking according to data from GUS (from the docs) written
   by Johan Zwiekhorst <jz@nfe.be>, for a more accurate recognition and
   to add some other unpackers (I wanted rar, as I have unrar, but added
   all the others; who knows, maybe it can be usefull for somebody else */
	if (memcmp(buf,"Rar!\32\007\000",7) == 0)
					     return unrar;  /* RAR */
	if (memcmp(buf,"HLSQZ",5) == 0)      return unsqz;  /* SQZ */
	if (memcmp(buf,"PK",2) == 0)         return unzip;  /* ZIP */
	if (memcmp(buf,"HPAK",4) == 0)       return unhpk;  /* HPK */
	if (memcmp(buf,"UC2\032",4) == 0)    return unuc2;  /* UC2 */
	if (memcmp(buf,"\2213HF",4) == 0)    return unhap;  /* HAP */
	if (memcmp(buf+20,"\334\247\304\375",4) == 0)
                                             return unzoo;  /* ZOO */
	if ((memcmp(buf+2,"-l",2) == 0) && (memcmp(buf+6,"-",1) == 0))
                                             return unlzh;  /* LHarc */
	if (memcmp(buf,"HA",2) == 0)         return unha;   /* HA  */
	if (memcmp(buf,"`\352",2) == 0)      return unarj;  /* ARJ */
	if (memcmp(buf,"\032Jar\033",5)==0)  return unjar;  /* JAR */		
	if (*buf == 0x1a)                    return unarc;  /* arc */
	logerr("Unknown compress scheme in file %s",S(fn));
	return NULL;
}
