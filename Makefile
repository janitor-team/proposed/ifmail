# Copyright (c) Eugene G. Crosser, 1993-1997
# Top-level makefile for ifmail package

include CONFIG

TARFILE = ifmail-${VERSION}.tar.gz
DIRS = iflib ifgate ifcico
ifeq (yes,${USE_NLS})
DIRS += po
endif

all install depend man:
	for d in ${DIRS}; do (cd $$d && echo $$d && ${MAKE} $@) || exit; done;

directories:
	for i in ${BINDIR} `dirname ${CONFIGFILE}` ${MAPTABDIR} \
		`dirname ${DEBUGFILE}` `dirname ${LOGFILE}` ; do \
		if [ ! -d $${i} ]; then \
		mkdir -p $${i}; \
		chown ${OWNER}.${GROUP} $${i}; \
		chmod 0750 $${i}; \
		fi; \
	done
	chmod 0755 ${BINDIR}
	chmod 0755 ${MAPTABDIR}

install_config: directories
	${INSTALL} -o ${OWNER} -g ${GROUP} -m 0640 misc/config `dirname ${CONFIGFILE}`
	${INSTALL} -o ${OWNER} -g ${GROUP} -m 0640 misc/Areas `dirname ${CONFIGFILE}`
	for i in misc/maptabs/* ; do \
	${INSTALL} -o ${OWNER} -g ${GROUP} -m 0644 $${i} ${MAPTABDIR} ; \
	done

clean:
	rm -f .filelist ${TARFILE}
	for d in ${DIRS}; do (cd $$d && echo $$d && ${MAKE} $@) || exit; done;


tar:	${TARFILE}

${TARFILE}:	filelist
	cd ..; ${TAR} cvTf ifmail/.filelist - | gzip >ifmail/${TARFILE}

filelist:
	echo ifmail/Makefile >.filelist
	echo ifmail/CONFIG >>.filelist
	echo ifmail/README >>.filelist
	echo ifmail/README.charset >>.filelist
	echo ifmail/README.first >>.filelist
	echo ifmail/LSM >>.filelist
	echo ifmail/LSM.TX >>.filelist
	echo ifmail/misc >>.filelist
	echo ifmail/Changelog.TX >>.filelist
	echo ifmail/Credits.TX >>.filelist
	for d in ${DIRS}; do (cd $$d && echo $$d && ${MAKE} filelist && \
				cat filelist >>../.filelist) || exit; done;
