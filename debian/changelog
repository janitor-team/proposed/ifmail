ifmail (2.14tx8.10-26) unstable; urgency=medium

  * Fixed the dh overrides by splitting them between arch and indep.

 -- Marco d'Itri <md@linux.it>  Mon, 26 Aug 2019 18:58:25 +0200

ifmail (2.14tx8.10-25) unstable; urgency=medium

  * Fixed a segfault in the ifcico configuration parser.
    Thanks to Göran Weinholt for the help. (Closes: #872507)
  * Fixed the command line for tfido in inetd.conf. (Closes: #933938)
  * Fixed cross compilation, thanks to Helmut Grohne. (Closes: #929239)

 -- Marco d'Itri <md@linux.it>  Mon, 26 Aug 2019 01:08:43 +0200

ifmail (2.14tx8.10-24) unstable; urgency=medium

  * Updated the Vcs-* fields for salsa.
  * Merged the last NMU.

 -- Marco d'Itri <md@linux.it>  Sun, 17 Feb 2019 19:16:41 +0100

ifmail (2.14tx8.10-23.1) unstable; urgency=medium

  * Non-maintainer upload.
  * Use the compat level gdbm package, to fix build with new gdbm
    (This needs porting to new gdbm api, because the compat package will
     eventually disappear) (Closes: #888752)

 -- Gianfranco Costamagna <locutusofborg@debian.org>  Wed, 07 Feb 2018 10:34:20 +0100

ifmail (2.14tx8.10-23) unstable; urgency=medium

  * Made the builds reproducible, thanks to Chris Lamb. (Closes: #831629)
  * Fixed a FTBFS when built with dpkg-buildpackage -A, thanks to
    Santiago Vila. (Closes: #845117)
  * Depend on libfl-dev. (Closes: #846429)
  * Implemented FTS-5005, thanks to Markus Reschke.
  * Updated patch portability_headers to fix a FTBFS on x32.

 -- Marco d'Itri <md@linux.it>  Mon, 26 Dec 2016 04:40:59 +0100

ifmail (2.14tx8.10-22) unstable; urgency=medium

  * Added patch fix_bsyname to fix the bsyname() declaration.
    (Closes: #749383)
  * Use the bindnow hardening option.

 -- Marco d'Itri <md@linux.it>  Fri, 25 Jul 2014 01:16:38 +0200

ifmail (2.14tx8.10-21) unstable; urgency=low

  * Do not use perl4 modules. (Closes: #659423)
  * Added support for dpkg-buildflags and hardening.
  * Package converted to source format 3.0.

 -- Marco d'Itri <md@linux.it>  Mon, 14 May 2012 05:17:39 +0200

ifmail (2.14tx8.10-20) unstable; urgency=low

  * Package converted to quilt.
  * Fixed an occurrence of open(2) with no explicit mode. (Closes: #507044)
  * Stop the Makefiles from stripping the binaries. (Closes: #437198)
  * Removed some dead code from postinst. (Closes: #553289)
  * Fixed bashisms in some example scripts. (Closes: #530102)
  * Merged NMUs. (Closes: #408244, #413397, #435994, #493337)

 -- Marco d'Itri <md@linux.it>  Sun, 06 Dec 2009 21:22:31 +0100

ifmail (2.14tx8.10-19.4) unstable; urgency=low

  * Non-maintainer upload.
  * Fix the previous fix (435994), i.e. really exit 0 if LOGDIR doesn't exists
    in /etc/cron.weekly/ifmail (Closes: #493337)

 -- Filippo Giunchedi <filippo@debian.org>  Tue, 12 Aug 2008 16:52:04 +0200

ifmail (2.14tx8.10-19.3) unstable; urgency=low

  * Non-maintainer upload.
  * Exit 0 if logdir doesn't exists in /etc/cron.weekly/ifmail 
    (Closes: #435994)

 -- Filippo Giunchedi <filippo@debian.org>  Thu, 01 May 2008 16:45:05 +0200

ifmail (2.14tx8.10-19.2) unstable; urgency=high

  * Non-maintainer upload.
  * High-urgency upload for RC bugfix.
  * Add missing dependency on update-inetd to ifcico, and fix the 
    maintainer script handling to only add entries on a new install and 
    only remove them on purge.  Closes: #413397.

 -- Steve Langasek <vorlon@debian.org>  Sun, 18 Mar 2007 01:42:26 -0700

ifmail (2.14tx8.10-19.1) unstable; urgency=high

  * Non-maintainer upload.
  
  * Set urgency to high since it fixes an RC bug.

  * Added missing dep on adduser (Closes: #408244)

 -- Bastian Venthur <venthur@debian.org>  Wed, 31 Jan 2007 15:11:24 +0100

ifmail (2.14tx8.10-19) unstable; urgency=low

  * Updated dependency to exim4. (Closes: #228568)
  * Fixed gcc 4.0 FTBFS bugs. (Closes: #286428)

 -- Marco d'Itri <md@linux.it>  Sat, 16 Jul 2005 12:13:34 +0200

ifmail (2.14tx8.10-18) unstable; urgency=medium

  * Switched from libgdbmg1-dev to libgdbm-dev.
  * Switched from varargs.h to stdarg.h, patch courtesy of Matt Kraai.
    (Closes: #195439)

 -- Marco d'Itri <md@linux.it>  Sat, 14 Jun 2003 13:47:32 +0200

ifmail (2.14tx8.10-17) unstable; urgency=low

  * Patched flagexp.y and ifcico/Makefile to work with the new flex release
    (Closes: #191190).

 -- Marco d'Itri <md@linux.it>  Thu,  1 May 2003 13:27:47 +0200

ifmail (2.14tx8.10-16) unstable; urgency=medium

  * Modified flagexp.y to work with the new bison release (Closes: #164230).

 -- Marco d'Itri <md@linux.it>  Thu, 17 Oct 2002 02:22:03 +0200

ifmail (2.14tx8.10-15) unstable; urgency=low

  * Use gdbm-ndbm.h instead of ndbm.h (Closes: #162440).

 -- Marco d'Itri <md@linux.it>  Mon,  7 Oct 2002 21:13:17 +0200

ifmail (2.14tx8.10-14) unstable; urgency=medium

  * Added build depend on debhelper (Closes: #123708).
  * Added link for ifnews.8.gz (Closes: #92583).
  * Merged some code from Andrey Smirnov to support KOI8-R.

 -- Marco d'Itri <md@linux.it>  Sun, 30 Dec 2001 18:22:37 +0100

ifmail (2.14tx8.10-13) unstable; urgency=low

  * Added -DDO_NEED_TIME (Closes: #91914).

 -- Marco d'Itri <md@linux.it>  Wed, 28 Mar 2001 00:03:07 +0200

ifmail (2.14tx8.10-12) unstable; urgency=low

  * Added support for the inn2 package.
  * Added build dependencies (Closes: #58063).
  * Removed suidregister support.

 -- Marco d'Itri <md@linux.it>  Mon, 26 Mar 2001 16:02:09 +0200

ifmail (2.14tx8.10-11) frozen unstable; urgency=medium

  * Y2K patch (important!).
  * Porting fixes (Closes: #55252, #57394).
  * Fixes bug in /usr/doc/ifcico/examples/ifpoll (Closes: #56832).
  * Added Build-Depends line and updated to last policy.

 -- Marco d'Itri <md@linux.it>  Wed,  9 Feb 2000 12:22:24 +0100

ifmail (2.14tx8.10-10.1) frozen unstable; urgency=low

  * Non-maintainer upload (binary-only) for alpha
  * For some unclear reason `alpha' was removed from the list of arches...
  * Some minor patches to help a smooth compile. Diff reported to BTS

 -- Paul Slootman <paul@debian.org>  Tue, 25 Jan 2000 22:29:29 +0100

ifmail (2.14tx8.10-10) unstable; urgency=low

  * Updated the path of ifmail.mc to work with potato sendmail.

 -- Marco d'Itri <md@linux.it>  Sat,  8 Jan 2000 13:38:33 +0100

ifmail (2.14tx8.10-9) unstable; urgency=low

  * New features from ifmail-2.14-dev3.diff.gz: ACLs.
  * Fixed ndbm.h path with glibc 2.1 (closes: #51385).
  * Compiled with glibc 2.1 (closes: #44761).
  * Removed bashisms in debian/rules (closes: #51388).

 -- Marco d'Itri <md@linux.it>  Sun, 28 Nov 1999 12:30:34 +0100

ifmail (2.14tx8.10-8) unstable; urgency=low

  * RELAXED is not #defined anymore. Use -f on the iftoss command line
    if you need this feature.
  * PARANOID now is a config file option.
  * Disabled ModemHangup and ModemAfterCall: they don't work on linux.
  * ifcico hand patched to cm.alpha-4.1.
  * Got setproctitle() from netkit-telnet.
  * New feature from ifmail-2.10.os: sentmode keyword.

 -- Marco d'Itri <md@linux.it>  Mon,  1 Nov 1999 16:35:05 +0100

ifmail (2.14tx8.10-7) unstable; urgency=low

  * Now rnews logs each article submitted.
  * Messages with an unparseable date field will have a X-Bad-Date header
    in Usenet and a WARNING kludge when gated back.

 -- Marco d'Itri <md@linux.it>  Sun, 31 Oct 1999 13:22:14 +0100

ifmail (2.14tx8.10-6) unstable; urgency=low

  * Now the package is FHS compliant.
  * Misc fixes (closes: #42360, #44761).

 -- Marco d'Itri <md@linux.it>  Sun, 17 Oct 1999 12:44:51 +0200

ifmail (2.14tx8.10-5) unstable; urgency=low

  * debian/rules rewritten to separately build binary-arch and binary-indep
    packages (closes: #42492).
  * Added powerpc arch (closes: #42119).
  * Does not add anymore duplicate aliases (closes: 43133). 
  * Added restampolder configuration option.

 -- Marco d'Itri <md@linux.it>  Sun, 29 Aug 1999 19:59:13 +0200

ifmail (2.14tx8.10-4) unstable; urgency=low

  * ifmail does not depend anymore on news and mail packages (closes: #42354).
  * Removed bashism from ifmail postinst (closes: #42360).

 -- Marco d'Itri <md@linux.it>  Tue,  3 Aug 1999 11:53:21 +0200

ifmail (2.14tx8.10-3) unstable; urgency=low

  * Now depends on "perl | perl5".

 -- Marco d'Itri <md@linux.it>  Thu, 29 Jul 1999 18:47:09 +0200

ifmail (2.14tx8.10-2) unstable; urgency=low

  * Patched for glibc 2.1 (fixes #38362).
  * Added postfix configuration info.

 -- Marco d'Itri <md@linux.it>  Tue, 27 Jul 1999 19:39:28 +0200

ifmail (2.14tx8.10-1) unstable; urgency=low

  * New upstream release.
  * Added exim and postfix tips in README.Debian.

 -- Marco d'Itri <md@linux.it>  Fri,  2 Apr 1999 16:29:49 +0200

ifmail (2.14tx8.9-0) unstable; urgency=low

  * New upstream release.

 -- Marco d'Itri <md@linux.it>  Thu,  3 Sep 1998 19:37:46 +0200

ifmail (2.14tx8.8-2.1) unstable; urgency=low

  * non-maintainer (binary-only) upload for Alpha
  * patch to disable NEED_TRAP on Alpha. Perhaps NEED_TRAP is overkill
    for a production package (i.e. debian package) anyway?
  * 'chown ftn.news' in debian/rules is bogus, as user 'ftn' doesn't exist
    until the package is installed. ftn replaced with 64000 which is hardwired
    in postinst.

 -- Paul Slootman <paul@debian.org>  Tue, 25 Aug 1998 19:15:14 +0200

ifmail (2.14tx8.8-2) unstable; urgency=low

  * Applied fix for glibc 2.1 by Hartmut Koptein (fixes Bug#25724)

 -- Marco d'Itri <md@linux.it>  Sat, 15 Aug 1998 03:33:42 +0200

ifmail (2.14tx8.8-1) unstable; urgency=low

  * Applied "modemaftercall" patch from Pablo

 -- Marco d'Itri <md@linux.it>  Thu, 16 Jul 1998 20:16:50 +0200

ifmail (2.14tx8.8) unstable; urgency=low

  * New upstream release
  * Fixed bug in postinst
  * Fixed MSGID conversion bug in upstream 2.14tx8.8

 -- Marco d'Itri <md@linux.it>  Sun, 14 Jun 1998 16:36:08 +0200

ifmail (2.13tx8.7-2) frozen unstable; urgency=low

  * Compiled for glibc2 and moved to hamm/comm

 -- Marco d'Itri <md@linux.it>  Tue, 17 Mar 1998 12:45:29 -0800

ifmail (2.13tx8.7-1) unstable; urgency=low

  * Fixed segmentation fault on broken ^AMSGID.
  * Fixed double ^APID bug.

 -- Marco d'Itri <md@linux.it>  Thu, 12 Mar 1998 19:31:24 +0100

ifmail (2.13tx8.7) unstable; urgency=low

  * New upstream release

 -- Marco d'Itri <md@linux.it>  Mon, 26 Jan 1998 10:52:58 +0100

ifmail (2.12tx8.6-4) unstable; urgency=low

  * Package splitted in ifmail, ifcico and ifgate.

 -- Marco d'Itri <md@linux.it>  Mon, 29 Dec 1997 16:39:25 +0100

ifmail (2.12tx8.6-3) unstable; urgency=low

  * Fixed buggy postinst
  * Removed from postinst the broken format converter of /etc/ifmail/Areas

 -- Marco d'Itri <md@linux.it>  Sun, 28 Dec 1997 13:58:28 +0100

ifmail (2.12tx8.6-2) unstable; urgency=low

  * Removed old dynamic ftn user for ftn.ftn static user and group (#15155).
  * Removed changelog from /usr/doc/ftn

 -- Marco d'Itri <md@linux.it>  Thu, 9 Oct 1997 23:12:58 +0200

ifmail (2.12tx8.6-1) unstable; urgency=low

  * Now NLS support is compiled in
  * Postinst will automatically convert the format of /etc/ifmail/Areas
  * New upstream version
  * Removed my patches to message.c and defined ADD_PID in CONFIG

 -- Marco d'Itri <md@linux.it>  Thu, 9 Oct 1997 23:12:58 +0200

ifmail (2.11tx8.5-1) unstable; urgency=low

  * Patched message.c: at level 0 X-Newsreader is converted to ^APID.
  * Charset mapping tables are stored in /usr/lib/ifmail/maptabs/.
  * The full list of configured #defines can be found in README.Debian.
  * Initial Release.

 -- Marco d'Itri <md@linux.it>  Sun, 3 Aug 1997 20:26:53 +0200
