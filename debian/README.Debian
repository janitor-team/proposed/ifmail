ifmail-tx for DEBIAN
--------------------

The programs are compiled with the following #defines:

OPTS        = -DTERMAIL_HACK -DTPUT_STATUS_HACK -DADD_PID -DLEVEL=0 \
		-DDONT_REGATE -DSLAVE_SENDS_NAK_TOO \
		-DRNEWSB -DJE \
		-DRESTAMP_OLD_POSTINGS=14 -DBELEIVE_ZFIN=1 \
		-DHAS_TCP -DAREAS_HACKING \
		-DRESTAMP_FUTURE_POSTINGS -DFSCHTML -DMACHIGAI \
		-DALLOW_RETURNPATH -DGATEBAU_MSGID \
		-DDIRTY_CHRS \
		-DAREAS_NUMERAL_COMMENTS

You will probably want to add ftn to the dialout group:
# adduser ftn dialout


IMPORTANT: Debian specific patches
----------------------------------

The new config option "restampolder" can be used to configure the number
of days after which echomail is restamped.

The new config option "paranoid" makes iftoss refuse tossing packets
with wrong packet password.

The first character of the argument of the new config option "sentmode"
will be used as the transfert mode in ?lo files ("^" is the default).

rnews logs each article submitted so innreport can count them.

Messages with an unparseable date field will have a X-Bad-Date header
in Usenet and a WARNING kludge when gated back.

The config options ModemHangup and ModemAfterCall are ignored because
linux does not allow I/O on an a TTY after hangup.

setproctitle() in ifcico is enabled and ps will show the state of the
call.

=============================================================================
From the dev3 patch:

ifgate/attach.c:
               Changed transit attach handling. Now transit attached
               files are always killed after having been sent to a remote
               system.

iflib/falists.c:
               Commented out all debug() calls for performance reasons:
               profiling had shown 50% time spent in debug() calls from
               falists.c.


From the amdk patch (whatever that means...):

# define -DHIDDEN - �������� ������ Hidden � Override.
Override z:nnnn/nn <phone | -> [TXY | CM]
Hidden z:nnnn/nn <phone | -> [TXY | CM]


From ifmail-2.14.os-p7:

28. Override UTxy flag - option NoTxy in config. For example:
    options (address 2:5020/99999) NoTxy
    options (address 2:5020/99999 & time Any2300-0530,Any0730-1000) Call
    options (address 2:5020/99999 & time Any0530-0730,Any1000-2300) NoCall
    (Sergey@p500.f1354.n5020.z2.fidonet.org)


The EXPERIMENTAL_EMSI code from ifcico-cm has been merged but is not
compiled in by default. I never tested it.

=============================================================================
If you use postfix you just have to add your AKAs to $mydestination and
enable the transport map by adding to /etc/postfix/main.cf:

transport_maps = hash:$config_directory/transport

Put in /etc/postfix/transport something like:

.z1.fidonet.org		ifmail:f680.n335
.z2.fidonet.org		ifmail:f680.n335
.z3.fidonet.org		ifmail:f680.n335
.z4.fidonet.org		ifmail:f680.n335
.z5.fidonet.org		ifmail:f680.n335
.z6.fidonet.org		ifmail:f680.n335

and don't forget to rebuild the database with postmap.

=============================================================================
If you use exim you can try these lines:

# add ftn to the trusted users list
trusted_users = mail:list:uucp:ftn

# transport
ftn:
  driver = pipe;
  user = nobody
  command = "/usr/lib/ifmail/ifmail -r${host} \
        \"(${original_local_part}@${original_domain})\""
  return_fail_output = true

# router
ftnhost:
  driver = domainlist,
  transport = ftn;
  route_file = /etc/exim.ftnhosts
  search_type = partial-lsearch

Remember to add your FTN AKAs to local_domains.

=============================================================================
If you use sendmail and sendmailconfig you will have to run it again after
ifmail is installed and it will automatically add the new mailers
"ifmail", "ifmail-h" and "ifmail-c" (for normal, hold and crash mail).
Then you will have to add to the mailertable some entries like ones in
examples/mailertable.

You can define FIDO_SMART_HOST in sendmail.mc to route all netmail to a
single node:

define(`FIDO_SMART_HOST', `f1.n2.z3.fidonet.org')

My rules will try hard to guess which addresses in the fidonet.org domain
are real internet machines that should be reached via the internet.
If some addresses are incorrectly sent to ifmail you can write them as
*.ip.fidonet.org, i.e. if you want to send mail to user@www.n1.z1.fidonet.org
you have to address it to user@www.n1.z1.ip.fidonet.org.

=============================================================================
If you use qmail you can try something like that:
control/virtualdomains:
.z1.fidonet.org:ifmail-ahb
.n335.z2.fidonet.org:ifmail-eagle
f680.n335.z2.fidonet.org:ifmail-direct
.f680.n335.z2.fidonet.org:ifmail-direct
.z3.fidonet.org:ifmail-ahb
.z4.fidonet.org:ifmail-ahb
.z5.fidonet.org:ifmail-ahb
.z6.fidonet.org:ifmail-ahb

users/append:
+ifmail-:ftn:64000:64000:/var/qmail/alias/ifmail:-::

~alias/.qmail-ahb-default:
| preline -dfr /usr/lib/ifmail/ifmail -r f206.n332 "($EXT2@$HOST)" || exit 100

~alias/.qmail-direct-default:
| preline -dfr /usr/lib/ifmail/ifmail -r "${HOST#p*.}" "($EXT2@$HOST)"

~alias/.qmail-directp-default:
| preline -dfr /usr/lib/ifmail/ifmail -r "$HOST" "($EXT2@$HOST)"

The last one is for your points. AHB is the name of my uplink (2:332/206).

=============================================================================
