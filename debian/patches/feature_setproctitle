setproctitle() in ifcico is enabled and ps will show the state of the call.

--- /dev/null
+++ b/iflib/setproctitle.c
@@ -0,0 +1,159 @@
+/*
+ * setproctitle implementation for linux.
+ * Stolen from sendmail 8.7.4 and bashed around by David A. Holland
+ */
+
+/*
+ * Copyright (c) 1983, 1995 Eric P. Allman
+ * Copyright (c) 1988, 1993
+ *	The Regents of the University of California.  All rights reserved.
+ *
+ * Redistribution and use in source and binary forms, with or without
+ * modification, are permitted provided that the following conditions
+ * are met:
+ * 1. Redistributions of source code must retain the above copyright
+ *    notice, this list of conditions and the following disclaimer.
+ * 2. Redistributions in binary form must reproduce the above copyright
+ *    notice, this list of conditions and the following disclaimer in the
+ *    documentation and/or other materials provided with the distribution.
+ * 3. All advertising materials mentioning features or use of this software
+ *    must display the following acknowledgement:
+ *	This product includes software developed by the University of
+ *	California, Berkeley and its contributors.
+ * 4. Neither the name of the University nor the names of its contributors
+ *    may be used to endorse or promote products derived from this software
+ *    without specific prior written permission.
+ *
+ * THIS SOFTWARE IS PROVIDED BY THE REGENTS AND CONTRIBUTORS ``AS IS'' AND
+ * ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
+ * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
+ * ARE DISCLAIMED.  IN NO EVENT SHALL THE REGENTS OR CONTRIBUTORS BE LIABLE
+ * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
+ * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS
+ * OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION)
+ * HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
+ * LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY
+ * OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
+ * SUCH DAMAGE.
+ *
+ * From: @(#)conf.c	8.243 (Berkeley) 11/20/95
+ */
+ /*
+ char setproctitle_rcsid[] =
+  "$Id: setproctitle.c,v 1.3 1997/05/19 12:58:15 dholland Exp $";
+ */
+
+#ifdef USE_SETPROCTITLE
+
+#include <stdlib.h>
+#include <string.h>
+#include <stdarg.h>
+#include <unistd.h>
+#include <stdio.h>
+
+#include "setproctitle.h"
+/*
+**  SETPROCTITLE -- set process title for ps
+**
+**	Parameters:
+**		fmt -- a printf style format string.
+**		a, b, c -- possible parameters to fmt.
+**
+**	Returns:
+**		none.
+**
+**	Side Effects:
+**		Clobbers argv of our main procedure so ps(1) will
+**		display the title.
+*/
+
+
+/*
+**  Pointers for setproctitle.
+**	This allows "ps" listings to give more useful information.
+*/
+
+static char **Argv = NULL;		/* pointer to argument vector */
+static char *LastArgv = NULL;		/* end of argv */
+static char Argv0[128];			/* program name */
+
+void
+initsetproctitle(int argc, char **argv, char **envp)
+{
+	register int i;
+	char *tmp;
+
+	/*
+        **  Move the environment so setproctitle can use the space at
+	**  the top of memory.
+	*/
+
+	for (i = 0; envp[i] != NULL; i++)
+		continue;
+	__environ = (char **) malloc(sizeof (char *) * (i + 1));
+	for (i = 0; envp[i] != NULL; i++)
+		__environ[i] = strdup(envp[i]);
+	__environ[i] = NULL;
+
+	/*
+	**  Save start and extent of argv for setproctitle.
+	*/
+
+	Argv = argv;
+	if (i > 0)
+		LastArgv = envp[i - 1] + strlen(envp[i - 1]);
+	else
+		LastArgv = argv[argc - 1] + strlen(argv[argc - 1]);
+
+	tmp = strrchr(argv[0], '/');
+	if (!tmp) tmp = argv[0];
+	else tmp++;
+	strncpy(Argv0, tmp, sizeof(Argv0));
+	Argv0[sizeof(Argv0) - 1] = 0;
+}
+
+void
+setproctitle(const char *fmt, ...)
+{
+	register char *p;
+	register int i;
+	static char buf[2048];
+	va_list ap;
+
+	p = buf;
+
+	/* print progname: heading for grep */
+	/* This can't overflow buf due to the relative size of Argv0. */
+	(void) strcpy(p, Argv0);
+	(void) strcat(p, ": ");
+	p += strlen(p);
+
+	/* print the argument string */
+	va_start(ap, fmt);
+	(void) vsnprintf(p, sizeof(buf) - (p - buf), fmt, ap);
+	va_end(ap);
+
+	i = strlen(buf);
+
+	if (i > LastArgv - Argv[0] - 2)
+	{
+		i = LastArgv - Argv[0] - 2;
+		buf[i] = '\0';
+	}
+	(void) strcpy(Argv[0], buf);
+	p = &Argv[0][i];
+	while (p < LastArgv)
+		*p++ = ' ';
+	Argv[1] = NULL;
+}
+
+#else
+
+void initsetproctitle(int argc, char **argv, char **envp)
+{}
+
+void setproctitle(const char *fmt, ...)
+{}
+
+#endif
+
--- /dev/null
+++ b/iflib/setproctitle.h
@@ -0,0 +1,4 @@
+/* Call this from main. */
+void initsetproctitle(int argc, char **argv, char **envp);
+
+void setproctitle(const char *fmt, ...);
--- a/iflib/Makefile
+++ b/iflib/Makefile
@@ -22,7 +22,7 @@ OBJS = lutil.o xutil.o ulock.o rfcdate.o
 		rdconfig.o ftn.o packet.o pktname.o bwrite.o \
 		bread.o getheader.o scanout.o matchaka.o atoul.o \
 		nodelock.o trap.o rfcaddr.o expipe.o callstat.o \
-		cspace.o setprocn.o \
+		cspace.o setproctitle.o \
 		charset.o mime.o \
 		lhash.o hash.o falists.o ${NEEDED}
 
@@ -33,7 +33,7 @@ SRCS = lutil.c xutil.c ulock.c rfcdate.c
 		bread.c getheader.c scanout.c matchaka.c usleep.c \
 		execute.c execsh.c signal.c regexpr.c atoul.c \
 		nodelock.c trap.c rfcaddr.c expipe.c callstat.c \
-		cspace.c setprocn.c ref.c \
+		cspace.c setproctitle.c ref.c \
 		charset.c mime.c \
 		cleanup_ref.c make_new_ref.c tmpfile.c \
 		dirent.c lhash.c hash.c falists.c
--- a/ifcico/ftsc.c
+++ b/ifcico/ftsc.c
@@ -8,6 +8,7 @@
 #include "ttyio.h"
 #include "statetbl.h"
 #include "config.h"
+#include "setproctitle.h"
 
 extern int master;
 extern int nodelock(faddr*);
@@ -30,7 +31,6 @@ extern int xmsndfiles(file_list*);
 extern int sendbark(void);
 extern int recvbark(void);
 extern void rdoptions(node*);
-extern int setproctitle(char*);
 
 static int rxftsc(void);
 static int txftsc(void);
@@ -40,12 +40,10 @@ static file_list *tosend;
 int rx_ftsc(void)
 {
 	int rc;
-	char cbuf[128];
 
 	loginf("start inbound ftsc session");
 
-	sprintf(cbuf,"-FTS-0001 (undefined yet) inbound");
-	setproctitle(cbuf);
+	setproctitle("-FTS-0001 (undefined yet) inbound");
 
 	session_flags |= SESSION_BARK;
 	rc=rxftsc();
@@ -65,13 +63,11 @@ int rx_ftsc(void)
 int tx_ftsc(void)
 {
 	int rc;
-	char cbuf[128];
 
 	loginf("start outbound ftsc session with %s",
 		ascfnode(remote->addr,0x1f));
 
-	sprintf(cbuf,"-FTS-0001 %s outbound",ascfnode(remote->addr,0x1f));
-	setproctitle(cbuf);
+	setproctitle("-FTS-0001 %s outbound",ascfnode(remote->addr,0x1f));
 
 	rc=txftsc();
 	if (rc)
@@ -272,7 +268,6 @@ SM_EDECL
 	FILE *fp;
 	faddr f,t;
 	fa_list **tmpl;
-	char cbuf[128];
 
 SM_START(recv_packet)
 
@@ -333,9 +328,8 @@ SM_STATE(scan_packet)
 			inbound=listinbound;
 		}
 
-		sprintf(cbuf,"-FTS-0001 %s inbound",
+		setproctitle("-FTS-0001 %s inbound",
 			ascfnode(remote->addr,0x1f));
-		setproctitle(cbuf);
 
 		tosend=create_filelist(remote,ALL_MAIL,1);
 		if (rc == 0) {SM_PROCEED(recv_file);}
--- a/ifcico/call.c
+++ b/ifcico/call.c
@@ -14,6 +14,7 @@
 #include "session.h"
 #include "callstat.h"
 #include "Txy.h"
+#include "setproctitle.h"
 
 extern int forcedcalls;
 extern char *forcedphone;
@@ -56,7 +57,6 @@ faddr *addr;
 	int speed;
 	char *portlist,*p,*q;
 	callstat *st;
-	char cbuf[128];
 
 	if ((nlent=getnlent(addr)) == NULL)
 	{
@@ -89,7 +89,7 @@ faddr *addr;
 
 	inbound=protinbound; /* master sessions are secure */
 
-	sprintf(cbuf,"ifcico calling %s (%s %s)",
+	setproctitle("ifcico calling %s (%s %s)",
 		ascfnode(&(nlent->addr),0x1f),
 #if defined(HAS_TCP) || defined(HAS_TERM)
 		inetaddr?"addr":"phone",
@@ -99,7 +99,6 @@ faddr *addr;
 #endif
 			forcedphone?forcedphone:
 				nlent->phone?nlent->phone:"<none>");
-	setproctitle(cbuf);
 
 	if ((nlent->phone || forcedphone
 #if defined(HAS_TCP) || defined(HAS_TERM)
--- a/ifcico/yoohoo.c
+++ b/ifcico/yoohoo.c
@@ -11,6 +11,7 @@
 #include "emsi.h"
 #include "nodelist.h"
 #include "version.h"
+#include "setproctitle.h"
 
 /*------------------------------------------------------------------------*/
 /* YOOHOO<tm> CAPABILITY VALUES                                           */
@@ -43,7 +44,6 @@ extern int rxdietifna(void);
 extern int txdietifna(void);
 extern void rdoptions(node*);
 extern int nodelock(faddr*);
-extern void setproctitle(char*);
 
 static int rxyoohoo(void);
 static int txyoohoo(void);
@@ -86,7 +86,6 @@ int rx_yoohoo(void)
 	char *pwd;
 	fa_list *tmp;
 	node *nlent;
-	char cbuf[128];
 
 	loginf("start inbound WaZOO session");
 
@@ -153,8 +152,7 @@ int rx_yoohoo(void)
 	}
 	if (rc) return rc;
 
-	sprintf(cbuf,"-WaZOO %s inbound",ascfnode(remote->addr,0x1f));
-	setproctitle(cbuf);
+	setproctitle("-WaZOO %s inbound",ascfnode(remote->addr,0x1f));
 
 	session_flags |= SESSION_WAZOO;
 	if (localcaps & DOES_HYDRA) return hydra(0);
@@ -176,7 +174,6 @@ int tx_yoohoo(void)
 	unsigned short capabilities;
 	char *pwd;
 	fa_list *tmp;
-	char cbuf[128];
 
 	loginf("start outbound WaZOO session");
 
@@ -211,8 +208,7 @@ int tx_yoohoo(void)
 	}
 	if (rc) return rc;
 
-	sprintf(cbuf,"-WaZOO %s outbound",ascfnode(remote->addr,0x1f));
-	setproctitle(cbuf);
+	setproctitle("-WaZOO %s outbound",ascfnode(remote->addr,0x1f));
 
 	session_flags |= SESSION_WAZOO;
 	if (capabilities & DOES_HYDRA) return hydra(1);
--- a/ifcico/emsi.c
+++ b/ifcico/emsi.c
@@ -11,6 +11,7 @@
 #include "emsi.h"
 #include "nodelist.h"
 #include "version.h"
+#include "setproctitle.h"
 
 #ifdef HAS_TCP
 #define LOCAL_PROTOS (PROT_ZMO | PROT_ZAP | PROT_HYD | PROT_TCP)
@@ -28,7 +29,6 @@ extern int rxwazoo(void);
 extern int txwazoo(void);
 extern unsigned INT16 crc16(char*,int);
 extern void rdoptions(node *);
-extern void setproctitle(char*);
 
 static int rxemsi(void);
 static int txemsi(void);
@@ -52,7 +52,6 @@ char *data;
 	fa_list *tmp,*tmr;
 	int denypw=0;
 	node *nlent=NULL;
-	char cbuf[128];
 
 	loginf("start inbound EMSI session");
 	emsi_local_lcodes=LCODE_RH1;
@@ -147,8 +146,7 @@ char *data;
 				"no common protocols");
 		return 0;
 	}
-	sprintf(cbuf,"-EMSI %s inbound",ascfnode(remote->addr,0x1f));
-	setproctitle(cbuf);
+	setproctitle("-EMSI %s inbound",ascfnode(remote->addr,0x1f));
 	if ((emsi_remote_opts & OPT_NRQ) == 0) session_flags |= SESSION_WAZOO;
 	else session_flags &= ~SESSION_WAZOO;
 #ifdef HAS_TCP
@@ -164,7 +162,6 @@ int tx_emsi(data)
 char* data;
 {
 	int rc;
-	char cbuf[128];
 
 	loginf("start outbound EMSI session");
 	emsi_local_lcodes=LCODE_PUA | LCODE_RH1;
@@ -215,8 +212,7 @@ char* data;
 			emsi_remote_protos?"traffic held":"no common protos");
 		return 0;
 	}
-	sprintf(cbuf,"-EMSI %s outbound",ascfnode(remote->addr,0x1f));
-	setproctitle(cbuf);
+	setproctitle("-EMSI %s outbound",ascfnode(remote->addr,0x1f));
 	emsi_local_protos &= emsi_remote_protos;
 	if ((emsi_remote_opts & OPT_NRQ) == 0) session_flags |= SESSION_WAZOO;
 	else session_flags &= ~SESSION_WAZOO;
--- a/ifcico/ifcico.c
+++ b/ifcico/ifcico.c
@@ -19,6 +19,7 @@
 #include "config.h"
 #include "version.h"
 #include "needed.h"
+#include "setproctitle.h"
 
 #if defined(SHORT_PID_T)
 #define pid_t short
@@ -43,7 +44,6 @@ fa_list *callall(void);
 int answer(char *);
 void mkdirs(char*);
 void setargspace(char**,char**);
-void setproctitle(char*);
 
 void usage(void)
 {
@@ -90,7 +90,7 @@ char *envp[];
 	int status;
 #endif
 
-	setargspace(argv,envp);
+	initsetproctitle(argc, argv, envp);
 
 #if defined(HAS_SYSLOG) && defined(CICOLOG)
 	logfacility=CICOLOG;
